package com.ghn.teltechdemo.remote

import com.ghn.teltechdemo.models.Number
import io.reactivex.Single

interface API  {

    //@GET("/api/checkNumber")
    fun checkNumber(number:String?): Single<Number>
}