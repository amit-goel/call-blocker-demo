package com.ghn.teltechdemo.remote

import com.ghn.teltechdemo.models.Number
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_DANGEROUS
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_SAFE
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_SUSPECT
import io.reactivex.Single

class MockLookupService : API {

    //mock dangerous or suspicious numbers
    private val blockedNumbers = listOf(
            Number("4259501212", SEVERITY_LEVEL_SUSPECT),
            Number("2539501212", SEVERITY_LEVEL_DANGEROUS)
    )

    override fun checkNumber(number: String?): Single<Number> {

        val foundBlocked = blockedNumbers.filter { it.phoneNumber == number }

        if (!foundBlocked.isEmpty()) {
            return Single.just(foundBlocked[0])
        }

        return Single.just(number?.let { Number(it, SEVERITY_LEVEL_SAFE) })
    }
}