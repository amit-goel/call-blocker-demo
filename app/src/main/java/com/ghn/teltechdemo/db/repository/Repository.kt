package com.ghn.teltechdemo.db.repository

import androidx.lifecycle.LiveData
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity
import com.ghn.teltechdemo.models.ItemRow
import io.reactivex.Flowable

interface Repository {

    fun getNumbers(database: AppDatabase?): LiveData<List<BlockedNumberEntity>>?
    fun deleteNumber(row: ItemRow, appDatabase: AppDatabase?)
    fun addNumber(number: String, appDatabase: AppDatabase?)
    fun getNumbersFlowable(database: AppDatabase?): Flowable<List<BlockedNumberEntity>>?
}