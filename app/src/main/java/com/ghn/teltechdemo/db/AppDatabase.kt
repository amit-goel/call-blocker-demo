package com.ghn.teltechdemo.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ghn.teltechdemo.db.dao.BlockedNumberDao
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity

@Database(entities = [(BlockedNumberEntity::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: AppDatabase? = null
        private const val DB_NAME = "teltech-db"

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME).build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }

    abstract fun blockedNumberDao(): BlockedNumberDao
}
