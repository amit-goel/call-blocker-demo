package com.ghn.teltechdemo.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity
import io.reactivex.Flowable

@Dao
interface BlockedNumberDao {
    @Query("SELECT * FROM blockedNumbers")
    fun loadAllNumbers(): LiveData<List<BlockedNumberEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(numbers: List<BlockedNumberEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(number: BlockedNumberEntity): Long

    @Query("DELETE FROM blockedNumbers WHERE id = :id")
    fun delete(id: Long?)

    @Query("SELECT * FROM blockedNumbers")
    fun getAll(): Flowable<List<BlockedNumberEntity>>

}
