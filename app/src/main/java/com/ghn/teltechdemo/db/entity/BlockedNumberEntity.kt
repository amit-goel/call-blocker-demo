package com.ghn.teltechdemo.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "blockedNumbers")
data class BlockedNumberEntity(@PrimaryKey(autoGenerate = true) var id: Long?,
                 @ColumnInfo(name = "number") val number: String,
                 @ColumnInfo(name = "severity") val severity: Int)