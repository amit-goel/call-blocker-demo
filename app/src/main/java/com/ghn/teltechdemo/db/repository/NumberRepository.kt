package com.ghn.teltechdemo.db.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity
import com.ghn.teltechdemo.models.ItemRow
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class NumberRepository : Repository {

    override fun getNumbers(database: AppDatabase?): LiveData<List<BlockedNumberEntity>>? {
        return database?.blockedNumberDao()?.loadAllNumbers()
    }

    override fun deleteNumber(row: ItemRow, appDatabase: AppDatabase?) {
        Observable.just(row)
                .subscribeOn(Schedulers.io())
                .doOnNext { appDatabase?.blockedNumberDao()?.delete(it.number.id) }
                .subscribeBy(onNext = {
                    Log.d("NumberRepository", "Deleted ${it.number.number} from blocked call list")
                })
    }

    override fun addNumber(number: String, appDatabase: AppDatabase?) {
        Observable.just(BlockedNumberEntity(null, number, 2))
                .subscribeOn(Schedulers.io())
                .doOnNext { appDatabase?.blockedNumberDao()?.insert(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = {
                    Log.d("TAG", "Inserted Number")
                })
    }

    override fun getNumbersFlowable(database: AppDatabase?): Flowable<List<BlockedNumberEntity>>? {
        return database?.blockedNumberDao()?.getAll()
    }
}