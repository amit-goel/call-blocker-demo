package com.ghn.teltechdemo.reciever

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.repository.NumberRepository

val ACTION_BLOCK = "com.ghn.teltech.demo.receiver.action.block"
val EXTRA_NUMBER = "number"

class MyBroadcastReceiver : BroadcastReceiver(){

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == ACTION_BLOCK) {
            val number = intent.extras?.getString(EXTRA_NUMBER)
            Log.d("MyBroadcastReceiver", "blocking $number")

            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            number?.hashCode()?.let { notificationManager.cancel(it)}

            val repo = NumberRepository()
            repo.addNumber(number!!, AppDatabase.getAppDataBase(context))
        }

    }

}
