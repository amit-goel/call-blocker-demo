package com.ghn.teltechdemo.reciever

import android.Manifest.permission.READ_CONTACTS
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.telephony.TelephonyManager
import androidx.core.app.ActivityCompat
import com.ghn.teltechdemo.models.Number
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_DANGEROUS
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_UNKOWN_CALLER
import com.ghn.teltechdemo.service.APILookupService
import com.ghn.teltechdemo.utils.Notifications
import com.ghn.teltechdemo.utils.PreferenceUtils
import com.ghn.teltechdemo.utils.TelephonyHelper

class IncomingCallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        if (intent.action == TelephonyManager.ACTION_PHONE_STATE_CHANGED) {
            try {
                val state = intent.getStringExtra(TelephonyManager.EXTRA_STATE)
                val number = intent.extras?.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)

                if(number != null) {
                    when (state) {
                        TelephonyManager.EXTRA_STATE_RINGING -> handleIncomingCall(context, number)
                        TelephonyManager.EXTRA_STATE_OFFHOOK -> return
                        TelephonyManager.EXTRA_STATE_IDLE -> return
                        else -> return
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun handleIncomingCall(context: Context, number: String?) {
        val formatNumber = if (number!!.length > 10) number.substring(number.length - 10) else number
        if(ActivityCompat.checkSelfPermission(context, READ_CONTACTS) == PackageManager.PERMISSION_GRANTED){
            if (PreferenceUtils.getBlockUnknown(context) && !TelephonyHelper.isNumberContact(context, formatNumber)) {
                TelephonyHelper.handleFoundNumber(Number(formatNumber, SEVERITY_LEVEL_DANGEROUS), context)
                Notifications.showNotification(Number(formatNumber, SEVERITY_LEVEL_UNKOWN_CALLER), context)
                return
            }
        }

        val intent = Intent(context, APILookupService::class.java)
        intent.putExtra(TelephonyManager.EXTRA_INCOMING_NUMBER, formatNumber)
        context.startService(intent)
    }

}
