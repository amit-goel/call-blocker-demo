package com.ghn.teltechdemo.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity
import com.ghn.teltechdemo.db.repository.NumberRepository
import com.ghn.teltechdemo.models.ItemRow
import java.util.*

class MainActivityViewModel : ViewModel() {

    private val numberRepository by lazy { NumberRepository() }
    private var blockedNumberData: LiveData<List<BlockedNumberEntity>>? = null

    fun blockedNumbers(appDatabase: AppDatabase?): LiveData<List<BlockedNumberEntity>>? {

        if (blockedNumberData == null) {
            blockedNumberData = MutableLiveData<List<BlockedNumberEntity>>()
            blockedNumberData = numberRepository.getNumbers(appDatabase)
        }
        return blockedNumberData
    }

    fun deleteBlockedNumber(row: ItemRow, appDatabase: AppDatabase?) {
        numberRepository.deleteNumber(row, appDatabase)
    }

    fun addRandomNumberToBlock(appDatabase: AppDatabase?) {
        val random = Random()
        val number = StringBuilder("5")
        for (i in 0..8) {
            number.append(random.nextInt(9))
        }
        numberRepository.addNumber(number.toString(), appDatabase)

    }

}