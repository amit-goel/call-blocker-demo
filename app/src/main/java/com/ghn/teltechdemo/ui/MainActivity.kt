package com.ghn.teltechdemo.ui

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.Intent.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ghn.teltechdemo.R
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.entity.BlockedNumberEntity
import com.ghn.teltechdemo.models.ItemRow
import com.ghn.teltechdemo.utils.PreferenceUtils
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.*


private const val PERMISSION_CODE: Int = 2

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        ItemAdapter.UpdateListener {

    private var adapter: ItemAdapter? = null
    private val viewModel: MainActivityViewModel by lazy { ViewModelProviders.of(this).get(MainActivityViewModel::class.java) }
    private val database by lazy { AppDatabase.getAppDataBase(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handlePermissions()

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setUpRecyclerView()
        initViewModel()

        fab.setOnClickListener { startActivity(Intent(this@MainActivity, AddNumberActivity::class.java)) }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    private fun handlePermissions() = checkPermissions(getCallPermissions())

    private fun getCallPermissions(): Array<String> {
        var permissions = arrayOf(
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_CALL_LOG)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            permissions = permissions.plus(Manifest.permission.ANSWER_PHONE_CALLS)
        return permissions
    }

    private fun checkPermissions(permissions: Array<String>) {
        val filtered = permissions.filter {
            ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
        }

        if (!filtered.isEmpty())
            ActivityCompat.requestPermissions(this, filtered.toTypedArray(), PERMISSION_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_CODE -> {
                for (i in 0 until permissions.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        when (permissions[i]) {
                            Manifest.permission.READ_CONTACTS -> PreferenceUtils.setBlockUnknown(this, true)
                        }
                    } else {
                        when (permissions[i]) {
                            Manifest.permission.READ_CONTACTS -> {
                                val autoSwitch = nav_view.menu.findItem(R.id.nav_block_non_contacts).actionView as SwitchCompat
                                autoSwitch.isChecked = false
                            }
                            else -> Snackbar.make(coordinatorLayout, R.string.snackbar_warning, Snackbar.LENGTH_LONG).show()
                        }
                    }
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun setUpRecyclerView() {
        adapter = ItemAdapter(this)
        val manager = LinearLayoutManager(this)
        rv_blocked_numbers.layoutManager = manager
        rv_blocked_numbers.adapter = adapter

        val dividerItemDecoration = DividerItemDecoration(rv_blocked_numbers.context, manager.orientation)
        rv_blocked_numbers.addItemDecoration(dividerItemDecoration)
    }

    private fun initViewModel() {
        viewModel.blockedNumbers(AppDatabase.getAppDataBase(this))?.observe(this, renderItems())
    }

    private fun renderItems(): Observer<List<BlockedNumberEntity>> {
        return Observer { numbers ->
            if (!numbers.isEmpty())
                tv_add_numbers.visibility = View.GONE
            val rows = ArrayList<ItemRow>()
            for (number in numbers) {
                rows.add(itemRow(number))
            }
            adapter?.update(rows)
        }
    }

    private fun itemRow(number: BlockedNumberEntity): ItemRow {
        val formatted = "(${number.number.substring(0, 3)}) ${number.number.substring(3, 6)}-${number.number.substring(6)}"
        return ItemRow(BlockedNumberEntity(number.id, formatted, number.severity))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val autoSwitch = nav_view.menu.findItem(R.id.nav_block_non_contacts).actionView as SwitchCompat
        autoSwitch.isChecked = PreferenceUtils.getBlockUnknown(this)
        autoSwitch.setOnCheckedChangeListener { _, isChecked -> handleClick(isChecked) }
        return true
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener,
                                    cancelListener: DialogInterface.OnClickListener? = null) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .setCancelable(false)
                .create()
                .show()
    }

    private fun handleClick(isChecked: Boolean) {
        val filtered: List<String> = getCallPermissions().filter {
            when (it) {
                Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE -> {
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                }
                Manifest.permission.READ_CALL_LOG, Manifest.permission.ANSWER_PHONE_CALLS -> {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED &&
                                ContextCompat.checkSelfPermission(this, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED
                    }else {
                        ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED
                    }
                }
                else -> false
            }
        }

        filtered.forEach {
            if (isChecked) {
                handleSwitchUnset()
                val groupedPerms = when(it){
                    Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE -> {
                        arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE)
                    }
                    Manifest.permission.READ_CALL_LOG, Manifest.permission.ANSWER_PHONE_CALLS -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                            arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.ANSWER_PHONE_CALLS)
                        }else {
                            arrayOf(Manifest.permission.READ_CALL_LOG)
                        }
                    }
                    else -> return
                }

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, it)) {

                    showMessageOKCancel(getString(R.string.call_permission_warning),
                            DialogInterface.OnClickListener { _, _ ->
                                ActivityCompat.requestPermissions(this, groupedPerms, PERMISSION_CODE)
                            },
                            DialogInterface.OnClickListener { _, _ -> })
                    return
                }
                ActivityCompat.requestPermissions(this, groupedPerms, PERMISSION_CODE)
            }
            return
        }

        val hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            if (isChecked) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                    showMessageOKCancel(getString(R.string.contacts_permission_warning),
                            DialogInterface.OnClickListener { _, _ ->
                                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_CODE)
                            },
                            DialogInterface.OnClickListener { _, _ -> handleSwitchUnset() })
                    return
                }
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_CODE)
            }
            return
        }
        PreferenceUtils.setBlockUnknown(this, isChecked)
    }

    private fun handleSwitchUnset() {
        val autoSwitch = nav_view.menu.findItem(R.id.nav_block_non_contacts).actionView as SwitchCompat
        autoSwitch.isChecked = false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_share -> shareAppDetails()
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun shareAppDetails() {
        val sb = StringBuilder()
        sb.append("Hi, I am using this awesome call blocking app. I like this and I want you to check it out.\n\n")
        sb.append(getString(R.string.play_store_link) + this.baseContext.packageName)

        val myShareIntent = Intent().apply {
            action = ACTION_SEND
            putExtra(EXTRA_SUBJECT, getString(R.string.email_subject_text))
            putExtra(EXTRA_TEXT, sb.toString())
            type = "text/plain"
        }

        startActivity(createChooser(myShareIntent, getString(R.string.select_to_choose_text)))
    }

    override fun deleteNumber(row: ItemRow) {
        viewModel.deleteBlockedNumber(row, database)
    }
}
