package com.ghn.teltechdemo.ui

import androidx.lifecycle.ViewModel
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.repository.NumberRepository

class AddNumberViewModel : ViewModel() {

    private val numberRepository by lazy { NumberRepository() }

    fun addNumberToBlock(appDatabase: AppDatabase?, number: String) {
        numberRepository.addNumber(number, appDatabase)
    }

}