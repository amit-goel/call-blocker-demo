package com.ghn.teltechdemo.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.ghn.teltechdemo.BR
import com.ghn.teltechdemo.R
import com.ghn.teltechdemo.models.ItemRow
import kotlinx.android.synthetic.main.item_row.view.*
import java.util.*

class ItemAdapter(val listener: UpdateListener?) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private val items = ArrayList<ItemRow>()

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        val deleteButton = binding.root.imageView2
        fun bind(row: ItemRow) {
            binding.setVariable(BR.item, row)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.item_row, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.deleteButton.setOnClickListener{listener?.deleteNumber(items[position])}
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun update(newItems: List<ItemRow>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<ItemRow> {
        return items
    }

    interface UpdateListener {
        fun deleteNumber(row: ItemRow)
    }
}
