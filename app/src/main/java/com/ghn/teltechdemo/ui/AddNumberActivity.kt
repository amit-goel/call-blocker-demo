package com.ghn.teltechdemo.ui

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.ghn.teltechdemo.R
import com.ghn.teltechdemo.db.AppDatabase
import kotlinx.android.synthetic.main.activity_add_number.*
import kotlinx.android.synthetic.main.content_add_number.*

class AddNumberActivity : AppCompatActivity() {

    private val viewModel: AddNumberViewModel by lazy {
        ViewModelProviders.of(this).get(AddNumberViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_number)
        setUpToolBar()
        phone_number_edit_text.addTextChangedListener(PhoneNumberFormattingTextWatcher())
        add_number_button.setOnClickListener { validateNumber() }
    }

    private fun setUpToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        supportActionBar?.title = "Add a Number"
    }

    private fun validateNumber() {
        val phoneNumber = phone_number_edit_text.text.toString().replace(Regex("[^\\d]"), "")
        if(phoneNumber.length != 10){
            phone_number_edit_text.error = getString(R.string.number_ten_digits_error_msg)
            return
        }

        viewModel.addNumberToBlock(AppDatabase.getAppDataBase(this), phoneNumber)
        finish()
    }

}
