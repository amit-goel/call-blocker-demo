package com.ghn.teltechdemo.utils

import android.content.ContentResolver
import android.content.Context
import android.os.Build
import android.provider.ContactsContract
import android.telecom.TelecomManager
import android.telephony.TelephonyManager
import com.android.internal.telephony.ITelephony
import com.ghn.teltechdemo.models.Number

object TelephonyHelper {

    fun handleFoundNumber(it: Number?, context: Context) {

        if (Build.VERSION.SDK_INT >= 28) {
            val telecomsManager = context.getSystemService(Context.TELECOM_SERVICE) as TelecomManager
            if (it?.severityLevel!! > 1)
                telecomsManager.endCall()
        } else {
            val telephonyService: ITelephony
            val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            try {
                val method = telephonyManager.javaClass.getDeclaredMethod("getITelephony")
                method.isAccessible = true
                telephonyService = method.invoke(telephonyManager) as ITelephony

                when (it?.severityLevel) {
                    1 -> telephonyService.silenceRinger()
                    2 -> telephonyService.endCall()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun isNumberContact(context: Context, number: String?): Boolean {
        val resolver: ContentResolver = context.contentResolver;
        val projection = arrayOf(ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Contacts._ID, ContactsContract.Contacts.HAS_PHONE_NUMBER)
        val c = resolver.query(ContactsContract.Data.CONTENT_URI, projection, null, null, null)

        if (c.count > 0) {
            while (c.moveToNext()) {
                val id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID))
                val phoneNumber = (c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))).toInt()

                if (phoneNumber > 0) {
                    val cursorPhone = resolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", arrayOf(id), null)

                    if (cursorPhone.count > 0) {
                        while (cursorPhone.moveToNext()) {
                            var phoneNumValue = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            phoneNumValue = phoneNumValue.replace("(", "")
                                    .replace(")", "")
                                    .replace(" ", "")
                                    .replace("-", "")
                                    .replace("+", "")

                            if (number == phoneNumValue)
                                return true
                        }
                    }
                    cursorPhone.close()
                }
            }
        }
        c?.close()
        return false
    }
}