package com.ghn.teltechdemo.utils

import android.app.Notification
import android.app.Notification.EXTRA_NOTIFICATION_ID
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import com.ghn.teltechdemo.R
import com.ghn.teltechdemo.models.Number
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_DANGEROUS
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_SUSPECT
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_UNKOWN_CALLER
import com.ghn.teltechdemo.reciever.ACTION_BLOCK
import com.ghn.teltechdemo.reciever.EXTRA_NUMBER
import com.ghn.teltechdemo.reciever.MyBroadcastReceiver
import com.ghn.teltechdemo.ui.MainActivity
import java.util.*


object Notifications{

    fun showNotification(num: Number, context: Context) {

        val title = when (num.severityLevel) {
            SEVERITY_LEVEL_SUSPECT -> context.getString(R.string.suspicious_number)
            SEVERITY_LEVEL_DANGEROUS -> context.getString(R.string.dangerous_number_incoming)
            SEVERITY_LEVEL_UNKOWN_CALLER -> context.getString(R.string.unknown_caller_blocked)
            else -> context.getString(R.string.safe_number)
        }

        val message = when (num.severityLevel) {
            SEVERITY_LEVEL_SUSPECT -> "Incoming Call from ${num.phoneNumber} Is Suspicious."
            SEVERITY_LEVEL_DANGEROUS -> "Incoming Call from ${num.phoneNumber} Is Dangerous and has been blocked."
            SEVERITY_LEVEL_UNKOWN_CALLER -> "A unknown user from has tried to contact you." + num.phoneNumber +
                    " The Call is blocked"
            else -> context.getString(R.string.call_appears_safe)
        }

        val notification = getNotification(title, message, context, num.phoneNumber)
        val mgr = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mgr.notify(num.phoneNumber.hashCode(), notification)
    }

    fun getNotification(title: String, message: String, context: Context, number: String = ""): Notification {

        createChannel(context)

        val notification: Notification
        val notifyIntent = Intent(context, MainActivity::class.java)

        notifyIntent.putExtra("title", title)
        notifyIntent.putExtra("message", message)
        notifyIntent.putExtra("notification", true)
        notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val blockIntent = Intent(context, MyBroadcastReceiver::class.java).apply {
            action = ACTION_BLOCK
            putExtra(EXTRA_NUMBER, number)
            putExtra(EXTRA_NOTIFICATION_ID, 0)
        }

        val dummyUniqueInt = Random().nextInt(543254)
        val blockPendingIntent = PendingIntent.getBroadcast(context, dummyUniqueInt, blockIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notification = NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_local_phone_black_24dp)
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                    .addAction(R.drawable.ic_delete_black_24dp, context.getString(R.string.notifcation_block_action), blockPendingIntent)
                    .setContentText(message)
                    .build()
        } else {

            notification = NotificationCompat.Builder(context)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_local_phone_black_24dp)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                    .setContentTitle(title)
                    .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                    .addAction(R.drawable.ic_delete_black_24dp, context.getString(R.string.notifcation_block_action), blockPendingIntent)
                    .setContentText(message)
                    .build()
        }
        return notification

    }

    private const val CHANNEL_ID = "samples.notification.ghn.com.CHANNEL_ID"
    private const val CHANNEL_NAME = "Sample Notification"

    private fun createChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library

            val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description = "notification channel description"
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            manager.createNotificationChannel(notificationChannel)
        }

    }
}