package com.ghn.teltechdemo.utils

import android.content.Context
import android.preference.PreferenceManager

object PreferenceUtils {
    private val PREF_BLOCK_UNKOWN = "PREF_BLOCK_UNKNOWN"

    fun getBlockUnknown(context: Context): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(PREF_BLOCK_UNKOWN, false)
    }

    fun setBlockUnknown(context: Context, block: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putBoolean(PREF_BLOCK_UNKOWN, block).apply()
    }
}