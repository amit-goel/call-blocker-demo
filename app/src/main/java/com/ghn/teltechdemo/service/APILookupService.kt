package com.ghn.teltechdemo.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.telephony.TelephonyManager.EXTRA_INCOMING_NUMBER
import android.util.Log
import com.ghn.teltechdemo.db.AppDatabase
import com.ghn.teltechdemo.db.repository.NumberRepository
import com.ghn.teltechdemo.models.Number
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_DANGEROUS
import com.ghn.teltechdemo.models.SEVERITY_LEVEL_UNKOWN_CALLER
import com.ghn.teltechdemo.remote.MockLookupService
import com.ghn.teltechdemo.utils.Notifications
import com.ghn.teltechdemo.utils.Notifications.showNotification
import com.ghn.teltechdemo.utils.TelephonyHelper
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class APILookupService : Service() {
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val notification = Notifications.getNotification("Sample", "Checking Incoming Number", this)
        startForeground(999, notification)

        val service = MockLookupService()
        val number = intent?.extras?.getString(EXTRA_INCOMING_NUMBER)
        val repository = NumberRepository()

        repository.getNumbersFlowable(AppDatabase.getAppDataBase(this))?.toObservable()
                ?.subscribeOn(Schedulers.io())
                ?.flatMap { it -> Observable.fromIterable(it).filter { it.number == number }.toList().toObservable() }
                ?.doOnNext { res ->
                    if (!res.isEmpty()) {
                        TelephonyHelper.handleFoundNumber(Number(number!!, SEVERITY_LEVEL_DANGEROUS), this)
                        Notifications.showNotification(Number(number, SEVERITY_LEVEL_UNKOWN_CALLER), this)
                        stopSelf()
                    } else {
                        service.checkNumber(number)
                                .subscribeOn(Schedulers.io())
                                .doOnSuccess {
                                    if (!it.phoneNumber.isBlank())
                                        TelephonyHelper.handleFoundNumber(it, this)
                                }
                                .subscribeBy(
                                        onSuccess = {
                                            if (it.severityLevel > 0)
                                                showNotification(it, this)
                                            stopSelf()
                                        },
                                        onError = {
                                            it.printStackTrace()
                                            stopSelf()
                                        }
                                )
                    }
                }
                ?.subscribeBy(onNext = { if (!it.isEmpty()) Log.d("APILookupService", it[0].number) },
                        onError = { it.printStackTrace() },
                        onComplete = { Log.d("APILookupService", "Done") }
                )

        return START_STICKY
    }


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        Log.i("MyService", "onCreate Service")
    }

    override fun onDestroy() {
        Log.i("MyService", "onDestroy Service")
        compositeDisposable.dispose()
    }

}
