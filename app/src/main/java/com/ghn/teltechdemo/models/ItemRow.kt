package com.ghn.teltechdemo.models

import com.ghn.teltechdemo.db.entity.BlockedNumberEntity

data class ItemRow(val number: BlockedNumberEntity)