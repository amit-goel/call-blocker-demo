package com.ghn.teltechdemo.models

val SEVERITY_LEVEL_SAFE = 0
val SEVERITY_LEVEL_SUSPECT = 1
val SEVERITY_LEVEL_DANGEROUS = 2
val SEVERITY_LEVEL_UNKOWN_CALLER = 3

data class Number(val phoneNumber: String, val severityLevel: Int)