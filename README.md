# Android Demo App To block phone calls

Assumptions and Caveats

+ This app will work with 10 digit US based phone numbers and will pattern match accordingly.
+ Due to non public nature of Telephony API on Android 7.x and 8.0 devices, this app will only block calls on < 6.0 devices and 9.0+ devices
+ A pop up system heads up display for incoming calls may appear briefly before the call is blocked.  More investigation may be required to prevent the system notification from showing.

This demo app provides the following feature set
 
+ Ability to work with telephony manage to detect incoming calls and take action
+ Block all non contact phone calls, via a settings option in nav drawer
+ Display notification for any suspicious or dangerous numbers
+ Ability to process and block calls in background when app is not running
+ Project Organized cleanly using Kotlin and uses Rxjava for reactive extensions
+ Mocked Web Service to check if an existing number is safe, based on phone numbers provided in the requirements
+ Material Design UI
+ Ability to add phone numbers to a User defined blocked call list.(Currently random number generated)
+ Room db persistence for User Defined Block Number list.
+ Notification for blocked calls with action button to add the blocked number to the DB 

The following features are slated for future development

+ SMS blocking feature for non contacts, unknown sender
+ CallerId lookup Service 
+ Improved Permission Handling
+ Ability to block International Numbers greater than 10 digits
+ Enhanced Unit and Integration tests
